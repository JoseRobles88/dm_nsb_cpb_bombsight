## Description
Automatic Fetching System:

Full bash shell version
 
bombsight.sh is a software developed to generate data per photometric band per Pos0 (optional: Pos1 and Pos2):

1.  Emptying trash from the machine.
2.  Deleting all files with extension .dat in current directory.
3.  Making directory for Pos0_JohnsonB,V,and R).
4.  Copying SBJohnson for Pos0 to each directory created by option l.
5.  Concatenating files with Pos0 and adding the tag Johnson-B.
6.  Concatenating files with Pos0 and adding the tag Johnson-V.
7.  Concatenating files with Pos0 and adding the tag Johnson-R.
8.  Finding and copying the NSB Pos0 for B,V,and R into current directory.
9.  Finding and copying the NSB Pos1 for B,V,and R into current directory..
10. Finding and copying the NSB Pos2 for B,V,and R into current directory..
11. Retrieving an unique URL from piggyback.sh  by date YYYYMMDD/.


## Note:

To display help; type in terminal: ./bombsight.sh -h 

Crontab executes bombsight.sh every day from 17:20 to 22:30 (local time: Madrid).

Software for Unix-like systems developed by:

Jose Robles, josrob01@ucm.es