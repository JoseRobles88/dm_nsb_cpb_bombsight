#!/bin/bash

while getopts ":drhplmnoqpabu:" opt; do

  case $opt in

    d)

      find . -name "*Pos0.dat" -exec cp '{}' ./ \;

      ;;

    a)

      sudo rm -R /Users/joserobles/.Trash
      ;;

    b)

      rm -rf ./*.dat

      ;;

    r)

      find . -name "*Pos1.dat" -exec cp '{}' ./ \;

      ;;

    p)

      find . -name "*Pos2.dat" -exec cp '{}' ./ \;

      ;;

    l)

      mkdir Pos0_JohnsonB
      mkdir Pos0_JohnsonV
      mkdir Pos0_JohnsonR

      ;;

    m)

      #cp -r *Pos0* sb_data_Pos0/NSB_Pos0/
      cp -r *SBJohnson_B* Pos0_JohnsonB
      cp -r *SBJohnson_V* Pos0_JohnsonV
      cp -r *SBJohnson_R* Pos0_JohnsonR

      ;;

    n)

      cd Pos0_JohnsonB
      cat SBJohnson_B* > z1.txt
      awk 'BEGIN{OFS="\t"}$2=" Johnson-B "$2' z1.txt > z1e.csv
   
      ;;

    o)
      cd Pos0_JohnsonV
      cat SBJohnson_V* > z2.txt
      awk 'BEGIN{OFS="\t"}$2=" Johnson-V "$2' z2.txt > z2e.csv
     
     ;;

    q)
      cd Pos0_JohnsonR
      cat SBJohnson_R* > z3.txt
      awk 'BEGIN{OFS="\t"}$2=" Johnson-R "$2' z3.txt > z3e.csv
     ;;

    u)

      SEL=$OPTARG

      wget -r -np http://147.96.21.177/$SEL

      ;;

    h)

      echo "Usage:"

      echo "-h                                      Display this help message."

      echo "-a                                      Emptying trash from the machine."

      echo "-b                                      Deleting all files with extension .dat in current directory."

      echo "-l <Directory>                          Making directory for Pos0_JohnsonB,V,and R)."

      echo "-m <Directory>                          Copying SBJohnson for Pos0 to each directory created by option l."
    
      echo "-n <Directory>                          Concatenating files with Pos0 and adding the tag Johnson-B."

      echo "-o <Directory>                          Concatenating files with Pos0 and adding the tag Johnson-V."

      echo "-q <Directory>                          Concatenating files with Pos0 and adding the tag Johnson-R."

      echo "-d <IP Directory>                       Finding and copying the NSB Pos0 for B,V,and R into current directory."

      echo "-r <IP Directory>                       Finding and copying the NSB Pos1 for B,V,and R into current directory.."

      echo "-p <IP Directory>                       Finding and copying the NSB Pos2 for B,V,and R into current directory.."

      echo "-u <DATE>                               Retrieving an unique URL from piggyback.sh  by date YYYYMMDD/."

      ;;

    \?)

      echo "Invalid option: -$OPTARG" >&2

  esac

done

shift $((OPTIND-1))
